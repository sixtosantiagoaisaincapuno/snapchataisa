//
//  ViewController.swift
//  SnapchatAisa
//
//  Created by Mac 05 on 5/27/21.
//  Copyright © 2021 atiquipa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn
import FirebaseDatabase


class IniciarSesionViewController: UIViewController  {
        
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Database.database().reference().child("usuarios").setValue("xxx")
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        Database.database().reference().child("/usuarios").observe(DataEventType.value, with: {(snapshot) in
            print ("hola")
            print (snapshot)
        })
    }
    
    @IBAction func iniciarSesionTapped(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTextField.text!, password:
        passwordTextField.text!) {(user, error) in
            print("Intentando Iniciar Sesión")
            
            if error != nil{
                let alerta = UIAlertController(title: "El usuario ", message: "usuario: \(self.emailTextField.text!) no existe, primero debe crearlo para acceder a la  aplicación", preferredStyle: .alert)
                
                let btnCrear = UIAlertAction(title: "Crear", style: .default, handler: {
                    (UIAlertAction) in
                    
                    self.performSegue(withIdentifier: "crearusuariosegue", sender: nil)
                })
                
                let btnCancelar = UIAlertAction(title: "Cancelar", style: .default, handler: {
                    (UIAlertAction) in
                    
                })
                
                
                alerta.addAction(btnCrear)
                alerta.addAction(btnCancelar)
                
                self.present(alerta, animated: true, completion: nil)
            }else{
                print("Inicio de Sesión Exitoso")
                self.performSegue(withIdentifier: "iniciarsesionsegue", sender: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="crearusuariosegue"{
            let pantalla2:CrearUsuarioViewController = segue.destination as! CrearUsuarioViewController
            pantalla2.nombreUsuario = emailTextField.text!
        }
    }
    
    @IBAction func iniciarSesionGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func crearUsuario(_ sender: Any) {
         self.performSegue(withIdentifier: "crearusuariosegue", sender: nil)
    }    
}

