//
//  CrearUsuarioViewController.swift
//  SnapchatAisa
//
//  Created by Mac 05 on 6/7/21.
//  Copyright © 2021 atiquipa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class CrearUsuarioViewController: UIViewController {
    
    
    @IBOutlet weak var usuarioTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var nombreUsuario=""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usuarioTextField.text=nombreUsuario
    }
       
    
    @IBAction func crearUsuario(_ sender: Any) {
        Auth.auth().createUser(withEmail: self.usuarioTextField.text!, password: self.passwordTextField.text!, completion: {(user,error) in
           
            if error != nil{
                print("Se presentó el siguiente error al crear el usuario: \(error)")
                
                let alerta = UIAlertController(title: "Error", message: "Erro al  crear al usuario \(self.usuarioTextField.text!)  \(error)", preferredStyle: .alert)
                let btnOK = UIAlertAction(title: "Aceptar", style: .default, handler: {(UIAlertAction) in
                })
                alerta.addAction(btnOK)
                self.present(alerta, animated: true, completion: nil)
                
            }else{
                print("El usuario fué creado exitosamente")
                
                
                
                Database.database().reference().child("usuarios").child(user!.user.uid).child("email").setValue(user!.user.email)
                
                let alerta = UIAlertController(title: "Creación de Usuario", message: "Usuario \(self.usuarioTextField.text!) se creo correctamente.", preferredStyle: .alert)
                let btnOK = UIAlertAction(title: "Aceptar", style: .default, handler: {(UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                })
                alerta.addAction(btnOK)
                self.present(alerta, animated: true, completion: nil)
            }
        })
        
    }    
    
    @IBAction func cancelarCreacion(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
