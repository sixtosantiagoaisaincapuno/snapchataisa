//
//  ListarViewController.swift
//  SnapchatAisa
//
//  Created by Mac 05 on 6/7/21.
//  Copyright © 2021 atiquipa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ListarViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
            
    @IBOutlet weak var listarUsuarios: UITableView!
        
    var usuarios:[Usuario] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listarUsuarios.delegate=self
        listarUsuarios.dataSource=self
        
        Database.database().reference().child("usuarios").observe(DataEventType.value, with: {(snapshot) in
            print(snapshot)
            
            let usuario = Usuario()
            usuario.email = (snapshot.value as! NSDictionary)["email"] as! String
            usuario.uid = snapshot.key
            self.usuarios.append(usuario)
            self.listarUsuarios.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usuarios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let usuario = usuarios[indexPath.row]
        cell.textLabel?.text = usuario.email
        return cell
    }
    
   
    
}
